#include<iostream>
#include <time.h>

int main()
{
	const int ROW = 5;
	const int COL = 6;

	int arr[ROW][COL];
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			arr[i][j] = i + j;
			std::cout << arr[i][j] << '\t';
		}
		std::cout << std::endl;
	}

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	std::cout << "Current day: " << buf.tm_mday << std::endl;

	int numberString = buf.tm_mday % ROW;
	std::cout << "Row index: " << numberString << std::endl;


	int sum = 0;
	for (int i = 0; i < ROW; i++)
	{
		sum += arr[numberString][i];
	}
	std::cout << "sum: " << sum << std::endl;
}

